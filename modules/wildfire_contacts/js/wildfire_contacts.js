/**
 * Implementation of Drupal behavior.
 */
Drupal.behaviors.wildfireContacts = function(context) {
    if (!Array.prototype.filter) {
        Array.prototype.filter = function (fun /*, thisp */) {
            "use strict";

            if (this === void 0 || this === null)
                throw new TypeError();

            var t = Object(this);
            var len = t.length >>> 0;
            if (typeof fun !== "function")
                throw new TypeError();

            var res = [];
            var thisp = arguments[1];
            for (var i = 0; i < len; i++) {
                if (i in t) {
                    var val = t[i]; // in case fun mutates this
                    if (fun.call(thisp, val, i, t))
                        res.push(val);
                }
            }

            return res;
        };
    }

    // Contacts and addresses related stuff.
    var addresses_list = $('textarea[name="email_addresses"]');
    var addresses = new Array();
    addresses = addresses_list.val().split(/\n/);
    addresses_list.change(addressesChange);    

    function addressesChange() {
        addresses = addresses_list.val().split(/[\s,;]+/).filter(function (e, i, a) { return !(e === '' || typeof e == 'undefined' || e === null); });
        addresses_list.val(addresses.join("\n"));
    }

    // Init email addresses text area
    addressesChange();

    // When name or email is clicked, or associated checkbox is checked, import into email addresses.
    if ($('#wildfire-contacts-list')) {
        $('div.contact-check').change(function() {
            var email = $(this).find('input').val();
            var checked = $(this).find('input').attr('checked');
            var address_found = $.inArray(email, addresses);
            if (checked && address_found == -1) {
                addresses.push(email);
                $(this).parent().addClass('checked');
            }
            else if (!checked && address_found > -1) {
                addresses.splice(address_found, 1);
                $(this).parent().removeClass('checked');
            }
            addresses_list.val(addresses.join("\n"));
        });
    }

    // Add section to Wildfire section manager.
    Drupal.behaviors.wildfire.sections.add('wildfire-share-contacts', 'wildfire-contacts');

    // Show Contacts section when coming back from Google OAuth.
    if ($('#wildfire-share-contacts').hasClass('active')) {
        $('#wildfire-contacts').css('display', 'block');
    }
};