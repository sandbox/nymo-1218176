<?php $css = drupal_get_path("module", "wildfire_contacts") . "/css/wildfire_contacts.css"; ?>
<?php drupal_add_css($css); ?>
<?php /* print '<pre>' . print_r($form, 1) . '</pre>'; */ ?>
<div id="wildfire-contacts">
  <div id="wildfire-contacts-address-wrapper">
    <div id="wildfire-contacts-address-form">
      <div>Enter your friends' email addresses or import addresses from your contact list.</div>
      <div id="wildfire-contacts-addresses">
        <?php print drupal_render($form['email_addresses']); ?>
      </div>
      <div id="wildfire-contacts-send">
        <?php print drupal_render($form['send']); ?>
      </div>
      <?php print drupal_render($form['google']['request_google_contacts']); ?>
    </div>
  </div>
  <?php if ($form['google']['google_contacts_list']): ?>
  <div id="wildfire-contacts-web-services-wrapper">
    <div id="wildfire-contacts-web-services">
      <!-- eventually, we'll have more ways to get contacts -->
      <?php print drupal_render($form['google']['google_contacts_list']); ?>
    </div>
  </div>
  <?php endif; ?>
</div>
