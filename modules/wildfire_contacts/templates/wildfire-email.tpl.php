<p>
  <?php print $tokens['referrer']; ?> invites you to join <?php print $tokens['site']; ?>!  Click <a href="<?php print $tokens['link']; ?>">here</a> to register.
  <?php if (!empty($tokens['message'])): ?>
  <div style="clear:both;"></div>
  <div>
    <i><?php print $tokens['referrer']; ?> says:</i>
  </div>
  <blockquote>
    <?php print $tokens['message']; ?>
  </blockquote>
  <?php endif; ?>
</p>
