<?php $css = drupal_get_path("module", "wildfire_contacts") . "/css/wildfire_contacts.css"; ?>
<?php drupal_add_css($css); ?>
<?php global $theme; ?>
<div id="wildfire-contacts-list-wrapper">
  <div>
    <h3><?php print t('Choose from your Google contacts:'); ?></h3>
  </div>
  <div id="wildfire-contacts-list">
    <?php $i = 0; ?>
    <?php foreach ($contacts as $contact): ?>
    <?php $check = "check-" . $i; ?>
    <div class="wildfire-contact">
      <div class="contact-check">
	<?php if ($contact->already_user): ?>
	<img src="<?php print _wildfire_variable_get("already_user_icon")/* drupal_get_path("theme", $theme) . "/favicon.ico" */; ?>" > <?php /*>*/ ?>
	<?php else: ?>
	<input id="<?php print $check; ?>" type="checkbox" value="<?php print $contact->email; ?>" />
	<?php endif; ?>
      </div>
      <div class="contact-name">
	<label for="<?php print $check; ?>"><?php print $contact->name; ?></label>
      </div>
      <div class="contact-email">
	<label for="<?php print $check; ?>"><?php print $contact->email; ?></label>
      </div>
    </div>
    <?php $i++; ?>
    <?php endforeach; ?>
  </div>
</div>
