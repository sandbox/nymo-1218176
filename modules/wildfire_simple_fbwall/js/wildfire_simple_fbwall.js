/**
 * Implementation of Drupal behavior.
 */
Drupal.behaviors.wildfireSimpleFBWall = function(context) {
    // Show/hide Simple FB Wall section. 
    $('#wildfire-share-simple-fbwall').click(function(e) {
        e.target = $('#wildfire-send-referrals');
        simple_fbwall_button_submit(e);
        Drupal.behaviors.wildfire.submit = false;
    });

    $('textarea[name="wildfire_message"]').change(function() {
        $('textarea#simple_fbwall_message').val($(this).val());
    });
};