/**
 * Implementation of Drupal behavior.
 */
Drupal.behaviors.wildfireTwitter = function(context) {
    // Cancel click event.
    Drupal.behaviors.wildfire.override.shareTwitter = true;
    
    // Add section to Wildfire section manager.
    Drupal.behaviors.wildfire.sections.add('wildfire-share-twitter', 'wildfire-twitter');

    // Show Twitter section when coming back from Twitter OAuth (not yet applicable).
    if ($('#wildfire-share-twitter').hasClass('active')) {
        $('#wildfire-twitter').css('display', 'block');
    }
};