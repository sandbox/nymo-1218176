<?php $css = drupal_get_path("module", "wildfire_twitter") . "/css/wildfire_twitter.css"; ?>
<?php drupal_add_css($css); ?>
<div id="wildfire-twitter">
  <div id="wildfire-twitter-options">
    <div id="wildfire-twitter-add-no-accounts">
      <?php print t("Add account"); ?>:
      <?php print drupal_render($form["add_account"]); ?>
    </div>
  </div>
  <?php print drupal_render($form); ?>
</div>
