<?php $css = drupal_get_path("module", "wildfire_twitter") . "/css/wildfire_twitter.css"; ?>
<?php drupal_add_css($css); ?>
<div id="wildfire-twitter">
  <div id="wildfire-twitter-options">
    <div id="wildfire-twitter-accounts">
      <h2><?php print t("Select account"); ?></h2>
      <?php print drupal_render($form["accounts"]); ?>
    </div>
    <div id="wildfire-twitter-add">
      <h2><?php print t("Add account"); ?></h2>
      <?php print drupal_render($form["add_account"]); ?>
    </div>
  </div>
  <div id="wildfire-twitter-tweet">
    <?php print drupal_render($form["tweet"]); ?>
  </div>
  <?php print drupal_render($form); ?>
</div>
