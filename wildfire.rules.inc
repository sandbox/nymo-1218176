<?php
// $Id$


/**
 * @file
 * Rules integration for the Wildfire module.
 */


/**
* Implementation of hook_rules_event_info().
* @ingroup rules
*/
function wildfire_rules_event_info() {
  return array(
    'wildfire_referral_registered' => array(
      'label' => t('A Wildfire referral registered'),
      'module' => 'wildfire',
      'arguments' => array(
        'referred_user' => array('type' => 'user', 'label' => t('The referred user.')),
        'referring_user' => array('type' => 'user', 'label' => t('The referring user.')),
      ),
    ),
  );
}


/**
 * implementation of hook_rules_condition_info()
 */
function wildfire_rules_condition_info() {
  return array(
    'wildfire_condition_referrals_total' => array(
      'label' => t('Number of referrals'),
      'arguments' => array(
        'referring_user' => array('type' => 'user', 'label' => t('Referrals.')),
      ),
      'module' => 'wildfire',
    ),
  );
}


/**
 * Comparison function
 *
 * @ingroup rules
 */
function wildfire_condition_referrals_total($referrer, $settings) {
  $referrals  = db_result(db_query('SELECT COUNT(referee) FROM {wildfire_referrals} WHERE status = 2 AND referrer = %d', $referrer->uid));
  $expected = $settings['referrals_total'];
  $match = ($referrals === $expected) ? TRUE : FALSE;
  return $match;
}

/**
 * Form to select terms to match against
 *
 * @param array $settings
 * @param array $form
 * @param array $form_state
 * @ingroup rules
 */
function wildfire_condition_referrals_total_form($settings, &$form, $form_state) {
  $form['settings']['referrals_total'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Number of referrals'),
    '#required'      => TRUE,
    '#description'   => t('Condition evaluates to TRUE if referrer has referred this number of referrals.'),
    '#default_value' => $settings['referrals_total'],
  );
}