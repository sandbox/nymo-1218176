<?php 
// $Id$

/**
 * User referral form.
 */
function wildfire_send_referrals($form_state) {
  $form = array();  
  
  drupal_add_js(drupal_get_path('module', 'wildfire') . '/js/wildfire.js');
  
  $referral_url = _wildfire_referral_url();

  $form['default']['form_header'] = array(
    '#value' => '<h1>' . t("Invite your friends!") . '</h1>',
  );
  
  $form['default']['plain_link'] = array(
    '#default_value' => $referral_url,
    '#type' => 'textfield',
    '#title' => _wildfire_variable_get('plain_link_title'),
  );

  $form['default']['wildfire_message'] = array(
    '#input_format' => '1',
    '#type' => 'textarea',
    '#default_value' => _wildfire_variable_get('message'),
    '#title' => _wildfire_variable_get('message_title'),
    '#resizable' => FALSE,
  );

  $form['default']['buttons'] = array(
    '#type' => 'markup',
  );

  $form['default']['buttons']['facebook_share'] = array(
    '#type' => 'submit',
    '#value' => t('Share on Facebook'),
    '#id' => 'wildfire-share-facebook',
    '#attributes' => array('class' => 'wildfire-share'),
  );

  $form['default']['buttons']['twitter_share'] = array(
    '#type' => 'submit',
    '#value' => t('Tweet on Twitter'),
    '#id' => 'wildfire-share-twitter',
    '#attributes' => array('class' => 'wildfire-share'),
    '#access' => _wildfire_variable_get('twitter_share'),
  );

  $form['default']['referral_url'] = array(
    '#type' => 'hidden',
    '#value' => $referral_url,
  );

  return $form;
}

function wildfire_send_referrals_validate(&$form, &$form_state) {
}

function wildfire_send_referrals_submit($form, &$form_state) {
  $values = $form_state['values'];
  $query = array(
    'u=' . $values['referral_url'],
    'url=' . $values['referral_url'],
    'text=' . preg_replace('/\s/', '+', ($values['social_network_message'])),
  );
  switch ($values['op']) {
    case $values['facebook_share']:
      drupal_goto('http://www.facebook.com/sharer.php?' . implode('&', $query));
      break;
    case $values['twitter_share']:
      drupal_goto('http://www.twitter.com/share?' . implode('&', $query));
      break;    
  }
}
