/**
 * Implementation of Drupal behavior.
 */
Drupal.behaviors.wildfire = function(context) {
    // Handle submit event.
    $('#wildfire-send-referrals').submit(function() { 
        if (!Drupal.behaviors.wildfire.submit) {
            // Reset.
            Drupal.behaviors.wildfire.submit = true;
            return false; 
        }

        return true;
    });

    // Defaults
    // - Submission is enabled.
    Drupal.behaviors.wildfire.submit = true;

    // - Sections for submodules (i.e. Contacts, Twitter)
    Drupal.behaviors.wildfire.sections = Drupal.behaviors.wildfire.sections || {
        // Holds sections identified by their trigger element.
        triggers: {},
        // Adds a new section.
        add: function (t, s) {
            Drupal.behaviors.wildfire.sections.triggers[t] = s;
            $('#' + t).click(function (e) {
                Drupal.behaviors.wildfire.submit = false;
                Drupal.behaviors.wildfire.sections.show($(this).attr('id'));
            });
        },
        // Hides every section, then toggles t's section's visibility.
        show: function (t) {
            var triggers = Drupal.behaviors.wildfire.sections.triggers;
            for (id in triggers) {
                if (id == t) continue;
                $('#' + id).removeClass('active');
                $('#' + triggers[id]).css('display', 'none');
            }
            if ($('#' + t).toggleClass('active').hasClass('active')) {
                $('#' + triggers[t]).css('display', 'block');
            }
            else {
                $('#' + triggers[t]).css('display', 'none');
            }
        }
    };

    // - Share links override disabled
    Drupal.behaviors.wildfire.override = Drupal.behaviors.wildfire.override || {};
    Drupal.behaviors.wildfire.override.shareFacebook = false;
    Drupal.behaviors.wildfire.override.shareTwitter = false;

    // Social network links
    $('#wildfire-share-facebook, #wildfire-share-twitter').click(function() {
        Drupal.behaviors.wildfire.submit = false;
        var referralUrl = $('input[name="referral_url"]').val();
        var msg = $('textarea[name="wildfire_message"]').val().replace(/\s/g, '+');
        var link = '';
        if ($(this).val().match(/facebook/i)) {
            if (Drupal.behaviors.wildfire.override.shareFacebook) return;
            link = 'http://www.facebook.com/sharer.php?u=' + referralUrl + '&t=' + msg + '&title=' + msg;
        }
        else if ($(this).val().match(/twitter/i)) {
            if (Drupal.behaviors.wildfire.override.shareTwitter) {
                return;
            }
            link = 'http://www.twitter.com/share?text=' + msg + '&url=' + referralUrl;
        }

        if (link) {
            window.open(link);
        }
    });

    
};