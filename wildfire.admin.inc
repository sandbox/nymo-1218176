<?php
// $Id$

/**
 * @file 
 * Wildfire admin's interface.
 */

/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Settings form.
 */
function wildfire_admin_settings() {
  $form = array();

  $form['wildfire_url_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Base URL path'),
    '#default_value' => _wildfire_variable_get('url_prefix'),
    '#required' => TRUE,
    '#description' => t('Wildfire base url path.'),
  );

  $form['wildfire_invitation_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Registration form title'),
    '#default_value' => _wildfire_variable_get('invitation_title'),
    '#description' => t("Enter the title for the registration form visited by a referee."),
  );

  $form['wildfire_plain_link_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Plain link title'),
    '#default_value' => _wildfire_variable_get('plain_link_title'),
    '#description' => t("Enter the plain link's title."),
  );

  $form['wildfire_message_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Message title'),
    '#default_value' => _wildfire_variable_get('message_title'),
    '#description' => t("Enter the message's title."),
  );

  $form['wildfire_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Referral message'),
    '#default_value' => _wildfire_variable_get('message'),
    '#description' => t("Enter the default referral message."),
  );
  
  $form['wildfire_twitter_share'] = array(
    '#type' => 'checkbox',
    '#title' => t("Enable Twitter sharing"),
    '#default_value' => _wildfire_variable_get('twitter_share'),
    '#description' => t("Some browsers (e.g. Firefox) perform url encoding on top of an already encoded url, breaking the message passed to Twitter.  So far, apostrophes and pound signs have been problematic."),
  );

  // Validate each field...
  $form = system_settings_form($form);
  $form['buttons']['#weight'] = 100;
  // ...then rebuild menu if necessary.
  $form['#submit'][] = 'wildfire_admin_settings_submit';

  return $form;
}

function wildfire_admin_settings_validate(&$form, &$form_state) {
  
}

/**
 * Sets url prefix and rebuild cache if necessary.
 */
function wildfire_admin_settings_submit($form, $form_state) {
  if ($form_state['values']['wildfire_url_prefix'] != $form['wildfire_url_prefix']['#default_value']) {
    menu_rebuild();
    drupal_set_message('Menu rebuilt.');
  }
}