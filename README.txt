Drupal Wildfire module:
-----------------------
Author - Jerome Truong (nymo)
Requires - Drupal 6

Overview:
---------
Wildfire allows users to send referrals by email or post on their 
Facebook wall and tweet on Twitter.

Installation:
-------------
Wildfire only enables Facebook and Twitter sharing at the basic level.
Enabling its submodules enables:
- email
- import Google contacts (requires oauth_google)
- post directly to Facebook (requires simple_fb_wall)
- tweet directly on Twitter (requires twitter, must be patched for OAuth 3.x)

Contributions:
--------------
* This module was developed by me, Matt Vance, Alan Johson, and Jerry Philips.
* Design and functionality aspect elements were drawn from Dropbox's referral program.

Last updated:
------------
// $Id$


