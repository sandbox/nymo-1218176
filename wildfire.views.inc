<?php

/**
 * @file
 */

/**
 * Implementation of hook_views_data().
 *
 */
function wildfire_views_data() {
  $data = array();
  $data['wildfire_referrals'] = array (
    'table' =>
    array (
      'group' => 'Wildfire referrals',
      'base' =>
      array (
        'field' => 'wid',
        'title' => 'Wildfire referrals',
        'help' => 'Data table',
        'weight' => 10,
      ),
      'join' =>
      array (
        'users' =>
        array (
          'left_field' => 'uid',
          'field' => 'referrer',
        ),
      ),
      'join' =>
      array (
        'users' =>
        array (
          'left_field' => 'uid',
          'field' => 'referee',
        ),
      ),    ),
    'wid' =>
    array (
      'title' => 'Wid',
      'help' => 'Wid',
      'field' =>
      array (
        'handler' => 'views_handler_field_numeric',
        'help' => 'Wid',
        'click sortable' => true,
      ),
      'filter' =>
      array (
        'handler' => 'views_handler_filter_numeric',
        'allow empty' => true,
        'help' => 'Filter on <em>Wid</em>',
      ),
      'argument' =>
      array (
        'handler' => 'views_handler_argument_numeric',
        'help' => 'Wid',
      ),
      'sort' =>
      array (
        'handler' => 'views_handler_sort',
        'help' => 'Sort by <em>Wid</em>',
      ),
    ),
    'referrer' =>
    array (
      'title' => t('Referrer'),
      'help' => t('Referrer'),
      'relationship' =>
      array (
        'handler' => 'views_handler_relationship',
        'base' => 'users',
        'field' => 'uid',
        'label' => t('Referrer'),
      ),
      'filter' =>
      array (
        'handler' => 'views_handler_filter_user_name',
      ),
      'argument' =>
      array (
        'handler' => 'views_handler_argument_numeric',
        'help' => 'Referrer',
      ),
      'sort' =>
      array (
        'handler' => 'views_handler_sort',
        'click sortable' => true,
      ),
    ),
    'referee' =>
    array (
      'title' => t('Referee'),
      'help' => t('Referee'),
      'relationship' =>
      array (
        'handler' => 'views_handler_relationship',
        'base' => 'users',
        'field' => 'uid',
        'label' => t('Referee'),
      ),
      'filter' =>
      array (
        'handler' => 'views_handler_filter_user_name',
      ),
      'argument' =>
      array (
        'handler' => 'views_handler_argument_numeric',
        'help' => 'Referee',
      ),
      'sort' =>
      array (
        'handler' => 'views_handler_sort',
        'click sortable' => true,
      ),
    ),
    'mail' =>
    array (
      'title' => 'Mail',
      'help' => 'Mail',
      'field' =>
      array (
        'handler' => 'views_handler_field',
        'help' => 'Mail',
        'click sortable' => true,
      ),
      'filter' =>
      array (
        'handler' => 'views_handler_filter_string',
        'allow empty' => true,
        'help' => 'Filter on <em>Mail</em>',
      ),
      'argument' =>
      array (
        'handler' => 'views_handler_argument_string',
        'help' => 'Mail',
      ),
      'sort' =>
      array (
        'handler' => 'views_handler_sort',
        'help' => 'Sort by <em>Mail</em>',
      ),
    ),
    'sent' =>
    array (
      'title' => 'Sent',
      'help' => 'Sent',
      'field' =>
      array (
        'handler' => 'views_handler_field_date',
        'help' => 'Sent',
        'click sortable' => true,
      ),
      'filter' =>
      array (
        'handler' => 'views_handler_filter_date',
        'allow empty' => true,
        'help' => 'Filter on <em>Sent</em>',
      ),
      'argument' =>
      array (
        'handler' => 'views_handler_argument_date',
        'help' => 'Sent',
      ),
      'sort' =>
      array (
        'handler' => 'views_handler_sort_date',
        'help' => 'Sort by <em>Sent</em>',
      ),
    ),
    'updated' =>
    array (
      'title' => 'Updated',
      'help' => 'Updated',
      'field' =>
      array (
        'handler' => 'views_handler_field_date',
        'help' => 'Updated',
        'click sortable' => true,
      ),
      'filter' =>
      array (
        'handler' => 'views_handler_filter_date',
        'allow empty' => true,
        'help' => 'Filter on <em>Updated</em>',
      ),
      'argument' =>
      array (
        'handler' => 'views_handler_argument_date',
        'help' => 'Updated',
      ),
      'sort' =>
      array (
        'handler' => 'views_handler_sort_date',
        'help' => 'Sort by <em>Updated</em>',
      ),
    ),
    'status' =>
    array (
      'title' => 'Status',
      'help' => 'Status',
      'relationship' =>
      array (
        'handler' => 'views_handler_relationship',
        'base' => 'wildfire_statuses',
        'field' => 'sid',
        'label' => t('Status'),
      ),
      'field' =>
      array (
        'handler' => 'views_handler_field_numeric',
        'help' => 'Status',
        'click sortable' => true,
      ),
      'filter' =>
      array (
        'handler' => 'views_handler_filter_numeric',
        'allow empty' => true,
        'help' => 'Filter on <em>Status</em>',
      ),
      'argument' =>
      array (
        'handler' => 'views_handler_argument_numeric',
        'help' => 'Status',
      ),
      'sort' =>
      array (
        'handler' => 'views_handler_sort',
        'help' => 'Sort by <em>Status</em>',
      ),
    ),
  );

  $data['wildfire_codes'] = array (
    'table' =>
    array (
      'group' => 'Wildfire codes',
    ),
    'uid' =>
    array (
      'title' => 'Uid',
      'help' => 'Uid',
      'field' =>
      array (
        'handler' => 'views_handler_field_numeric',
        'help' => 'Uid',
        'click sortable' => true,
      ),
      'filter' =>
      array (
        'handler' => 'views_handler_filter_numeric',
        'allow empty' => true,
        'help' => 'Filter on <em>Uid</em>',
      ),
      'argument' =>
      array (
        'handler' => 'views_handler_argument_numeric',
        'help' => 'Uid',
      ),
      'sort' =>
      array (
        'handler' => 'views_handler_sort',
        'help' => 'Sort by <em>Uid</em>',
      ),
    ),
    'code' =>
    array (
      'title' => 'Code',
      'help' => 'Code',
      'field' =>
      array (
        'handler' => 'views_handler_field',
        'help' => 'Code',
        'click sortable' => true,
      ),
      'filter' =>
      array (
        'handler' => 'views_handler_filter_string',
        'allow empty' => true,
        'help' => 'Filter on <em>Code</em>',
      ),
      'argument' =>
      array (
        'handler' => 'views_handler_argument_string',
        'help' => 'Code',
      ),
      'sort' =>
      array (
        'handler' => 'views_handler_sort',
        'help' => 'Sort by <em>Code</em>',
      ),
    ),
  );

  $data['wildfire_statuses'] = array (
    'table' =>
    array (
      'group' => 'Wildfire statuses',
      'base' => 
      array (
        'field' => 'sid',
        'title' => 'Wildfire statuses',
        'help' => 'Wildfire statuses table',
        'weight' => 10,
      ),
    ),
    'label' =>
    array (
      'title' => 'Status',
      'help' => 'Status label',
      'field' =>
      array (
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
        'help' => 'Status label',
      ),
      'sort' =>
      array (
        'handler' => 'views_handler_sort',
        'help' => 'Sort by <em>Status</em>',
      ),
    ),
  );

  return $data;
}
